# Open Meal Planning

Open meal planning is a method of organizing and planning meals that is transparent and allows for flexibility and individual preferences. 
It typically involves making a list of ingredients and dishes that are available and allowing individuals to choose what they would like to eat for each meal, 
rather than having a predetermined meal plan for each day. This approach can help people save time and money, 
and can also be more inclusive and accommodating for individuals with dietary restrictions or preferences.


## Getting Started


### Prerequisites



### Installing



## Deployment


## Built With

  - [Contributor Covenant](https://www.contributor-covenant.org/) - Used
    for the Code of Conduct
  - [Creative Commons](https://creativecommons.org/) - Used to choose
    the license

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code
of conduct, and the process for submitting pull requests to us.

## Versioning

We use [Semantic Versioning](http://semver.org/) for versioning. For the versions
available, see the [tags on this
repository](https://github.com/PurpleBooth/a-good-readme-template/tags).

## Authors

  - [**Alitux**](https://gitlab.com/alitux/)

## License

This project is licensed under the [GNU GPL V3](LICENSE)


## Acknowledgments