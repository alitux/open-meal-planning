��    +      t  ;   �      �     �     �     �     �     �     �     �     �  !         "  #   *     N  
   T     _     k     x     �     �  
   �     �     �     �  	   �     �     �     �     �     �     �            &        A     I     N  	   S     ]     m     ~     �     �     �  D  �     �     �     �  	   �  	     	               &   $     K     S     r     y     �     �     �     �     �     �     �     �  	   �  
   �  
                        1     D     J     Q  (   b     �     �     �     �     �     �     �     �     �     �     (                               %   	                       *               &           )                "   #              '      
                   $          +                    !                     Abbreviation Abreviation Author Biweekly Confirm Cookbook Daily Delete Do you want to delete the recipe  English Here you will find all your recipes Image Ingredient Ingredients Instructions Meals per Day Monthly Name New Recipe Number of Persons Planner Portions Privacity Privacy Private Public Purchase Unit Purchase Units Ratio Recipe Shopping List Short Description (200 Characters Max) Spanish Type Unit Unit Base Unit Conversion Unit Conversions User Weekly unit units Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Alitux <alitux@disroot.org>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Abreviatura Abreviatura Autor Quincenal Confirmar Recetario Diario Borrar ¿Realmente quiere borrar esta receta? Inglés Aquí están todas tus recetas Imagen Ingrediente Ingredientes Instrucciones Comidas por día Mensual Nombre Nueva Receta Cantidad de Personas Planificador Porciones Privacidad Privacidad Privado Público Unidad de Compra Unidades de Compra Ratio Receta Lista de Compras Descripción Corta (200 Carácteres Max) Español Tipo Unidad Unidad Base Conversión de Unidad Conversión de Unidades Usuario Semanal unidad Unidades 