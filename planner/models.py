from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import gettext as _
from cookbook.models import Recipe

PLAN_TYPE = [
    ('daily', _('Daily')),
    ('weekly', _('Weekly')),
    ('biweekly', _('Biweekly')),
    ('monthly', _('Monthly'))
]


class Plan(models.Model):

    class Meta:
        verbose_name = _("Plan")
        verbose_name_plural = _("Plans")
    
    user = models.ForeignKey(
        User, verbose_name=_("User"), on_delete=models.CASCADE)
    name = models.CharField(_("Name"), max_length=50, blank=None)
    type = models.CharField(_("Type"), max_length=50,
                            choices=PLAN_TYPE, blank=None, default='weekly')
    qt_persons = models.IntegerField(
        _("Number of Persons"), blank=None, validators=[MinValueValidator(1)],default=1)
    qt_diary_meals = models.IntegerField(_("Meals per Day"), blank=None, validators=[
                                         MinValueValidator(1), MaxValueValidator(6)], default=2)
    short_description = models.CharField(
        _("Short Description"), max_length=200)

    def __str__(self):
        return f"{self.name}({self.type})"

    def get_absolute_url(self):
        return reverse("Plan_detail", kwargs={"pk": self.pk})


class PlanRecipe(models.Model):

    plan = models.ForeignKey("planner.Plan", verbose_name=_(
        "Plan"), on_delete=models.CASCADE)
    recipe = models.ForeignKey("cookbook.Recipe", verbose_name=_(
        "Recipe"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Plan Recipe")
        verbose_name_plural = _("Plan Recipes")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("PlanRecipes_detail", kwargs={"pk": self.pk})
