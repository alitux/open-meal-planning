from django.contrib import admin
from .models import Plan, PlanRecipe

class RecipeInline(admin.TabularInline):
    #Class for Inline add in Admin
    model = PlanRecipe

class PlanAdmin(admin.ModelAdmin):
    #Class for Inline add in Admin
    inlines = [RecipeInline]

admin.site.register(Plan, PlanAdmin)