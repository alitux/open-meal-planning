# from django.contrib import admin
from baton.autodiscover import admin
from django.urls import path, include
from rest_framework.authtoken import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('baton/',include('baton.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('api/auth/token', views.obtain_auth_token),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('', include('cookbook.urls')),
    path('', include('core.urls')),
]
