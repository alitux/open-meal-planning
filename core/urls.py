from django.urls import path, include, re_path
from .views import UnitAPIView

urlpatterns = [
    path("api/units/", UnitAPIView.as_view(), name='unit_api')
]
