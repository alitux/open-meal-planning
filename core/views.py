from django.shortcuts import render
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UnitSerializer
from .models import Unit
#API Views

class UnitAPIView(APIView):
    """
    Return all Units
    """
    def get(self, request, *args, **kwargs):
        #List all items
        units = Unit.objects.all()
        serializer = UnitSerializer(units, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)