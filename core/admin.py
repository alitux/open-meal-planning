from django.contrib import admin
from .models import Unit, Purchase_Unit, Units_Conversion
from import_export import resources
from import_export.admin import ImportExportModelAdmin

# Admin Site Titles
admin.site.site_header = "OMP Admin"
admin.site.site_title = "Admin"
admin.site.index_title = "OMP Admin"

#Register in Import Export
class Unit_Resource(resources.ModelResource):

    class Meta:
        model = Unit

class UnitAdmin(ImportExportModelAdmin):
    resource_class = Unit_Resource

    class Meta:
        model = Unit

class Unit_Purchase_Unit(resources.ModelResource):

    class Meta:
        model = Purchase_Unit

class PurchaseUnitAdmin(ImportExportModelAdmin):
    resource_class = Unit_Resource

    class Meta:
        model = Purchase_Unit

# Add models to Admin
admin.site.register(Unit, UnitAdmin)
#admin.site.register(Purchase_Unit, PurchaseUnitAdmin)
#admin.site.register(Units_Conversion)