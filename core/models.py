from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import MaxValueValidator, MinValueValidator

class Unit(models.Model):
    # Unidad de uso general
    name = models.CharField(_("Name"), max_length=50, blank=False)
    abbreviation = models.CharField(_("Abbreviation"), max_length=10, blank=False)
    #unit_purchase = models.ForeignKey("core.Purchase_Unit", verbose_name=_("Purchase Unit"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("unit")
        verbose_name_plural = _("units")

    def __str__(self):
        return f"{self.name}({self.abbreviation})"

    def get_absolute_url(self):
        return reverse("unit_detail", kwargs={"pk": self.pk})

class Purchase_Unit(models.Model):
    #Unidad de Compras

    name = models.CharField(_("Unit"), max_length=50)
    abbreviation = models.CharField(_("Abreviation"), max_length=50)

    class Meta:
        verbose_name = _("Purchase Unit")
        verbose_name_plural = _("Purchase Units")

    def __str__(self):
        return self.abbreviation

    def get_absolute_url(self):
        return reverse("purchase_unit_detail", kwargs={"pk": self.pk})

class Units_Conversion(models.Model):

    ratio = models.FloatField(_("Ratio"))
    unit_base = models.ForeignKey("core.Unit", verbose_name=_("Unit Base"), on_delete=models.CASCADE)
    unit_purchase = models.ForeignKey("core.Purchase_Unit", verbose_name=_("Purchase Unit"), on_delete=models.CASCADE)
    class Meta:
        verbose_name = _("Unit Conversion")
        verbose_name_plural = _("Unit Conversions")

    def __str__(self):
        return f"{self.unit.base}->{self.unit_purchase}({self.ratio})"

    def get_absolute_url(self):
        return reverse("unit_conversion_detail", kwargs={"pk": self.pk})

