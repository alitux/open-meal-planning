from rest_framework import serializers
from .models import Recipe, Ingredient
from core.models import Unit


class UnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unit
        fields = ["name","abbreviation"]

class IngredientSerializer(serializers.ModelSerializer):
    # unit = UnitSerializer(many=True, read_only=True)
    unit = serializers.StringRelatedField()

    class Meta:
        model = Ingredient
        fields = ['name','qt','unit']


class RecipeSerializer(serializers.ModelSerializer):
    ingredients = IngredientSerializer(many=True, read_only=False)  # Cambia read_only a False
    class Meta:
        model = Recipe
        fields = ['name', 'short_description', 'portions',
                  'instructions', 'privacity', 'ingredients']

    def create(self, validated_data):
        ingredients_data = validated_data.pop('ingredients', [])

        recipe = Recipe.objects.create(**validated_data)

        for ingredient_data in ingredients_data:
            # Extract 'unit_id' from ingredient_data
            unit_id = ingredient_data.pop('unit_id', None)
            
            # If 'unit_id' is provided, retrieve the corresponding Unit instance
            unit = None
            if unit_id:
                try:
                    unit = Unit.objects.get(pk=unit_id)
                except Unit.DoesNotExist:
                    raise serializers.ValidationError(f"Invalid unit_id: {unit_id}")

            # Create the Ingredient instance with the related Unit (if provided)
            Ingredient.objects.create(recipe=recipe, unit=unit, **ingredient_data)
