from django.contrib import admin
from .models import Recipe, Ingredient

class IngredientInline(admin.TabularInline):
    #Class for Inline add in Admin
    model = Ingredient

class RecipeAdmin(admin.ModelAdmin):
    #Class for Inline add in Admin
    inlines = [IngredientInline]

admin.site.register(Recipe,RecipeAdmin)
#admin.site.register(IngredientName)
admin.site.register(Ingredient)