from django.urls import path, include, re_path
from . import views
from .views import CookBookListView, RecipeUpdateView, RecipeDeleteView, IngredientListView, EditRecipe, NewRecipe
from .views import PublicRecipeListView
from .views import RecipeCreateAPIView, RecipeAPIView
urlpatterns = [
    path('', CookBookListView.as_view(), name='index'),
    path('cookbook/', CookBookListView.as_view(), name='cookbook'),
    path('public/', PublicRecipeListView.as_view(), name='publicookbook'),
    path('recipe/new',NewRecipe.as_view(),name='new_recipe'),
    path('recipe/<int:recipeid>', views.view_recipe, name='view_recipe'),
    path('recipe/edit/<int:pk>', EditRecipe.as_view(), name='edit_recipe'),
    path('recipe/delete/<int:pk>', RecipeDeleteView.as_view(), name='delete_recipe'),
    path('ingredients', IngredientListView.as_view(), name='ingredients'),
    path('api/recipes',RecipeAPIView.as_view()), 
    path('api/recipes/create',RecipeCreateAPIView.as_view()), 
]
