from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, ListView, UpdateView, DeleteView, View, CreateView
from django.forms.models import inlineformset_factory
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from .models import Recipe, Ingredient
from .forms import RecipeForm, PortionsForm, IngredientFormSet, IngredientForm
##Testing
from extra_views import InlineFormSetFactory, CreateWithInlinesView
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Div, Row, Field, ButtonHolder, Submit, Column


##API 
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from .serializers import RecipeSerializer, IngredientSerializer

## API Views
class RecipeAPIView(APIView):
    """
    Return all Recipe for login user.
    """
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request, *args, **kwargs):
        #List all items
        recipes = Recipe.objects.filter(user = request.user.id)
        serializer = RecipeSerializer(recipes, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class RecipeCreateAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]  # Asegura que el usuario esté autenticado

    def post(self, request, format=None):
        data = request.data.copy()
        data['user'] = request.user.id  # Establece el usuario autenticado como el creador de la receta
        serializer = RecipeSerializer(data=data)

        if serializer.is_valid():
            serializer.save(user = request.user)
            
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@method_decorator(login_required, name='dispatch')
class CookBookListView(ListView):
    # List all Recipes
    model = Recipe
    template_name = "cookbook.html"
    context_object_name = 'recipes'

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


@method_decorator(login_required, name='dispatch')
class RecipeUpdateView(UpdateView):
    model = Recipe
    form_class = RecipeForm
    template_name = "edit_recipe.html"
    success_url = reverse_lazy('cookbook')


@method_decorator(login_required, name='dispatch')
class RecipeDeleteView(DeleteView):
    model = Recipe
    template_name = 'recipe_confirm_delete.html'
    success_url = reverse_lazy('cookbook')

    def form_valid(self, form):
        deleted_recipe = self.get_object()
        response = super().form_valid(form)

        messages.success(self.request, f"Receta '{deleted_recipe.name}' eliminada con éxito.")

        return response

@method_decorator(login_required, name='dispatch')
class IngredientListView(ListView):
    model = Ingredient
    context_object_name = 'ingredients'
    template_name = "ingredients.html"

    def get_queryset(self):
        return model.objects.filter(user=self.request.user)


@login_required()
def view_recipe(request, recipeid=None):
    request.session['active_menu'] = 'cookbook'
    recipe = Recipe.objects.get(id=recipeid)
    portionsform = PortionsForm(instance=recipe)
    ingredients = Ingredient.objects.filter(recipe=recipeid)
    context = {'recipe': recipe, 'ingredients': ingredients,
               'portionsform': portionsform}
    return render(request, 'view_recipe.html', context)


class EditRecipe(View):

    success_url = 'view_recipe'

    def get(self, request, pk):
        request.session['active_menu'] = 'cookbook'
        recipe = Recipe.objects.get(id=pk)
        recipeform = RecipeForm(instance=recipe)
        user_recipe = request.user
        ingredientsform = IngredientFormSet(instance=recipe)
        ingredients_all = Ingredient.objects.filter(user=user_recipe)
        ingredientlastid = Ingredient.objects.last().id
        context = {'recipe': recipe,
                   'recipeform': recipeform,
                   'ingredientsform': ingredientsform,
                   'ingredient_list': ingredients_all,
                   'ingredientlastid': ingredientlastid
                   }
        return render(request, 'edit_recipe.html', context)

    def post(self, request, pk):
        recipe = Recipe.objects.get(id=pk)
        form1 = RecipeForm(request.POST, instance=recipe)
        form2 = IngredientFormSet(request.POST, instance=recipe)
        if form1.is_valid() and form2.is_valid():
            # procesa la información de los formularios
            form1.save()
            form2.save()
            return redirect('view_recipe', recipe.id)
        else:
            return render(request, 'view_recipe.html', {'recipe': recipe, 'ingredients': Ingredient.objects.filter(recipe=recipe.id)})


class NewRecipe(CreateView):
    model = Recipe
    form_class = RecipeForm
    template_name = 'edit_recipe.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['ingredient_formset'] = IngredientFormSet(self.request.POST)
        else:
            data['ingredient_formset'] = IngredientFormSet()
        
        data['ingredients_all'] = Ingredient.objects.all()
        return data
    def form_valid(self, form):
        context = self.get_context_data()
        form.instance.user = self.request.user #Asociar al usuario actual
        ingredient_formset = context['ingredient_formset']

        if form.is_valid() and ingredient_formset.is_valid():
            self.object = form.save()
            ingredient_formset.instance = self.object
            ingredient_formset.save()
            messages.success(self.request, '¡Receta creada correctamente!')
            return super().form_valid(form)
        else:
            return self.render_to_response(self.get_context_data(form=form))

class PublicRecipeListView(ListView):
    model = Recipe
    template_name = "public_recipes_list.html"
    paginate_by = 50

    def get_queryset(self):
        queryset = self.model.objects.filter(privacity="public")
        print(queryset)
        return queryset
    
    
