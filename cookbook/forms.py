from django.forms import ModelForm, TextInput, ChoiceField, CharField, NumberInput
from django.utils.translation import gettext as _
from django.forms import inlineformset_factory
from ckeditor.widgets import CKEditorWidget
from crispy_forms.helper import FormHelper
from .models import Recipe, Ingredient, Unit
from dal import autocomplete



class RecipeForm(ModelForm):

    instructions = CharField(widget=CKEditorWidget())
    class Meta:
        model = Recipe
        fields = ['name', 'short_description',
                  'portions', 'instructions', 'privacity']
        widgets = {
            'name': TextInput(attrs={'autocomplete': 'off'}),
            'short_description': TextInput(attrs={'autocomplete': 'off'}),
            'portions': NumberInput(attrs={'min': '1'}),

        }


class IngredientForm(ModelForm):

    class Meta:
        model = Ingredient
        fields = ['name','qt','unit']
        widgets = {
            'name': TextInput(attrs={'list': 'ingredient_list', 'autocomplete': 'off', 'placeholder': _('Ingredient')}),
            'qt': NumberInput(attrs={'placeholder': 'Quantity'}),
        }
        # labels = {
        #     'name':'',
        #     'qt':'',
        #     'unit':'',
        # }

class PortionsForm(ModelForm):

    class Meta:
        model = Recipe
        fields = ['portions']


IngredientFormSet = inlineformset_factory(
    Recipe, Ingredient, form=IngredientForm, extra=1, can_delete=True)
