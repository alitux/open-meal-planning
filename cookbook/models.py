from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from ckeditor.fields import RichTextField
from core.models import Unit

class Recipe(models.Model):
    PRIVACITY = [
        ('public',_('Public')),
        ('private',_('Private'))
    ]
    # Recipe Model
    user = models.ForeignKey(
        User, verbose_name=_("User"), on_delete=models.CASCADE)
    name = models.CharField(_("Name"), max_length=50, blank=False)
    short_description = models.CharField(_("Short Description (200 Characters Max)"), max_length=200)
    image = models.ImageField(_("Image"), upload_to=None, height_field=None,
                              width_field=None, max_length=None, blank=True)
    portions = models.IntegerField(
        _("Portions"), validators=[MinValueValidator(1)])
    instructions = RichTextField(blank=False)
    privacity = models.CharField(_("Privacity"), max_length=20, choices=PRIVACITY, default='private')
    class Meta:
        verbose_name = "recipe"
        verbose_name_plural = "recipes"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("view_recipe", kwargs={"recipeid": self.id})

    def get_ingredients(self, portions=1):
        # Get Ingredients
        ingredients = []
        for item in Ingredient.objects.filter(recipe=self.id):
            ingredients.append(
                {
                    'name': item.name.name,
                    'qt': round((item.qt/self.portions)*portions, 2),
                    'unit': item.unit.abbreviation
                }
            )

        # Return Ingredient of Recipe
        return ingredients


class Ingredient(models.Model):
    # Ingredient Model
    # user = models.ForeignKey(User, verbose_name=_(
    #     "User"), on_delete=models.CASCADE)
    recipe = models.ForeignKey(Recipe, verbose_name=_(
        "Recipe"), on_delete=models.CASCADE, related_name='ingredients')
    name = models.CharField(_("Name"), max_length=50)

    qt = models.FloatField(_("Quantity"), blank=False)
    unit = models.ForeignKey(
        Unit, verbose_name=_("Unit"), on_delete=models.CASCADE, related_name='unit')

    class Meta:
        verbose_name = _("Ingredient")
        verbose_name_plural = _("Ingredients")

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse("Ingredient_detail", kwargs={"pk": self.pk})

    def clean(self):

        self.name = self.name.strip().capitalize()
